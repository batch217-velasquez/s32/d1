let http = require("http");

let port = 4000;

let server = http.createServer((req, res) => {
	//HTTP METHOD - GET
	//Get method means that we will be retrieving or reading information

	if(req.url == "/items" && req.method == "GET"){
		res.writeHead(200, {"Content-Type" : "text/plain"});
		//Ends or the final output in the application/browser
		res.end("Data retrieved from the database.");
	}

	//HTTP METHOD - POST
	//POST method means that we will be inserting data in the server or DB
	if(req.url == "/items" && req.method == "POST"){
		res.writeHead(200, {"Content-Type" : "text/plain"});
		res.end("Data to be sent to the database.");
	}
});

server.listen(port);

console.log(`Server is running at localhost:${port}.`);